//
//  NotesTableViewController.swift
//  partyplannerprotest
//
//  Created by Baron Heinrich on 10/26/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {

    var SelectedValue:String!
    var notes = [String]()
    var noteList: [String: String] = [:];
    
    override func viewDidLoad() {
        self.tableView.backgroundColor = UIColor.purple
        super.viewDidLoad()

         print("the selected value is", SelectedValue)
        print("the selected Value casted", Int32(SelectedValue))
        
       
        
        
        //open Database
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("partyplanner.db")

        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "create table if not exists notes (id integer primary key, partyId integer, name varchar, FOREIGN KEY(partyId) REFERENCES partyList(id) )",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
    
        //query database
        _ = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "select * from notes where partyId = (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        
        let myNewerInt = Int32(SelectedValue!)
        
        if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error binding name: \(errmsg)")
        }
        
        
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let id = sqlite3_column_int64(statement, 0)
            let partyId = sqlite3_column_int64(statement, 1)
            
            print("id = \(id); ", terminator: "")
            print("partyId = \(partyId); ", terminator: "")
            
            if let name = sqlite3_column_text(statement, 2)
            {
                let nameString = String(cString: name)

                notes.append(nameString)
                noteList[nameString]=String(id)
                
                print("note name = \(nameString)")
            } else {
                print("name not found")
            }
            
            
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        statement = nil
        
        if(notes.isEmpty)
        {
            
            
            var db: OpaquePointer? = nil
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                print("error opening database")
            }
            
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)

        
            let guests = "Guests"
            let menu = "Menu"
            var statement: OpaquePointer? = nil
            
            if sqlite3_prepare_v2(db, "insert into notes (partyId,name) values(?,?)", -1, &statement, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error prepping statement: \(errmsg)")
            }
            
            let myNewInt = self.SelectedValue
            let myNewerInt = Int32(myNewInt!)
            ///bind as many times as you have variables going into table
            //important parts are statemnt, second integer, and name
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 2, guests , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            //only one step
            if sqlite3_step(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error stepping: \(errmsg)")
            }
            
            
            //only have one finalize
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing: \(errmsg)")
            }
            
            //reset statement
            statement = nil
            
            if sqlite3_prepare_v2(db, "insert into notes (partyId,name) values(?,?)", -1, &statement, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error prepping statement: \(errmsg)")
            }
            
            ///bind as many times as you have variables going into table
            //important parts are statemnt, second integer, and name
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 2, menu , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            //only one step
            if sqlite3_step(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error stepping: \(errmsg)")
            }
            
            
            //only have one finalize
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing: \(errmsg)")
            }
            
            //reset statement
            statement = nil
            

            if sqlite3_prepare_v2(db, "select * from notes where partyId = (?)", -1, &statement, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error preparing select: \(errmsg)")
            }
            
                        
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            
            
            while sqlite3_step(statement) == SQLITE_ROW {
                let id = sqlite3_column_int64(statement, 0)
                let partyId = sqlite3_column_int64(statement, 1)
                
                print("id = \(id); ", terminator: "")
                print("partyId = \(partyId); ", terminator: "")
                
                if let name = sqlite3_column_text(statement, 2)
                {
                    let nameString = String(cString: name)
                    
                    notes.append(nameString)
                    noteList[nameString]=String(id)
                    
                    print("note name = \(nameString)")
                } else {
                    print("name not found")
                }
                
                
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing prepared statement: \(errmsg)")
            }
            
            statement = nil
            
            
            self.tableView.reloadData()

            
        }

        print(notes)
        print(noteList)
        //query db for id of entry with partyName
        self.tableView.reloadData()
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func addButton(_ sender: UIBarButtonItem)
    {
        
        let alertController = UIAlertController(title: "Holiday Party Notes", message: "Add Note", preferredStyle: .alert)
        
        alertController.addTextField { (textfield:UITextField) in
            
        }
        
        let addAction = UIAlertAction(title: "Add", style: .default) { (action:UIAlertAction) in
            let textfield = alertController.textFields?.first
            
            
            let userText = textfield!.text!
            
            
            
            //connects and opens db
            let fileURL = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("partyplanner.db")
            var db: OpaquePointer? = nil
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK
            {
                print("error opening database")
            }
            
            
            if sqlite3_exec(db, "create table if not exists notes(id integer primary key autoincrement, partyId integer, name varchar, FOREIGN KEY(partyId) REFERENCES partyList(id))",nil,nil,nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error creating table: \(errmsg)")
            }
            
            
            
            
            //create the statement, prepare the statement, bind values to statement, prep st, finalize st
            
            //insert
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)

            var statement: OpaquePointer? = nil
            
            if sqlite3_prepare_v2(db, "insert into notes (partyId,name) values(?,?)", -1, &statement, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error prepping statement: \(errmsg)")
            }
            
            let myNewInt = self.SelectedValue
            let myNewerInt = Int32(myNewInt!)
            ///bind as many times as you have variables going into table
            //important parts are statemnt, second integer, and name
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 2, userText , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            //only one step
            if sqlite3_step(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error stepping: \(errmsg)")
            }
            
            
            //only have one finalize
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing: \(errmsg)")
            }
            
            //reset statement
            statement = nil
            
            
            
            if sqlite3_prepare_v2(db, "select * from notes where partyId = (?)", -1, &statement, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error preparing select: \(errmsg)")
            }
            
            
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            self.notes.removeAll()
            
            while sqlite3_step(statement) == SQLITE_ROW {
                let id = sqlite3_column_int64(statement, 0)
                let partyId = sqlite3_column_int64(statement, 1)
                
                print("id = \(id); ", terminator: "")
                print("partyId = \(partyId); ", terminator: "")
                
                if let name = sqlite3_column_text(statement, 2)
                {
                    let nameString = String(cString: name)
                    
                    self.notes.append(nameString)
                    self.noteList[nameString]=String(id)
                    self.tableView.reloadData()
                    
                    print("note name = \(nameString)")
                } else {
                    print("name not found")
                }
                
                
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing prepared statement: \(errmsg)")
            }

  

            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action: UIAlertAction) in
        }
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0
        {
            return notes.count
        }
        else
        {
            return 1//number of color accents
        }
    }
    


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (notes[indexPath.row]=="Guests")
        {
            performSegue(withIdentifier: "guestSegue", sender: self)
        }
        
        else
        {
            performSegue(withIdentifier: "showNote", sender: self)
        }

    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var cell = UITableViewCell() as! AleTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = notes[indexPath.item]
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        
        return cell
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let name = notes[indexPath.row]
            notes.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            //remove from DB
            
            
            //open Database
            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("partyplanner.db")
            
            
            
            var db: OpaquePointer? = nil
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                print("error opening database")
            }
            
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
            var statement: OpaquePointer? = nil
            
            if sqlite3_prepare_v2(db, "delete from notes where name = (?)", -1, &statement, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error preparing select: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 1, name, -1, SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("failure binding name delete: \(errmsg)")
            }
            
            //one step
            if sqlite3_step(statement) != SQLITE_DONE {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("failure deleting party: \(errmsg)")
            }
            
            
            //finalize & reset statement
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing prepared statement: \(errmsg)")
            }
            
            statement = nil
            
            //close db & set to nil
            if sqlite3_close(db) != SQLITE_OK {
                print("error closing database")
            }
            
            db = nil

        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
 

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
 */
 

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?)
   {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    
    //let cell = sender as! UITableViewCell
    //let selectedRow = tableView.indexPath(for: cell)!.row
    
    //let myString = String(selectedRow)
    
    //print ("myString:", myString)

    
    
    
    if segue.identifier == "guestSegue"
    {
        
        let guestDestination = segue.destination as? GuestChecklistTableViewController
        
        guestDestination?.SelectedParty = SelectedValue
        
    }
    
    else
    {
        
        let destination = segue.destination as? ChecklistTableViewController
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        let selectedNote = noteList[(currentCell.textLabel?.text)!]
        
        
        destination?.SelectedParty = SelectedValue
        destination?.SelectedNote = selectedNote
        
        
    }
    

   }
 

}
